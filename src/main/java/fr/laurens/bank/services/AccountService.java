package fr.laurens.bank.services;

import fr.laurens.bank.domain.AccountOperationLog;

import java.math.BigDecimal;

public interface AccountService {

    void deposit(String accountID, BigDecimal amount);

    void withdraw(String accountID, BigDecimal amount);

    AccountOperationLog generateAccountStatement(String accountID);

    String printAccountStatement(String accountID);
}
