package fr.laurens.bank.domain;

import java.math.BigDecimal;

public interface BalanceChangingOperations {

    void deposit(BigDecimal amount) throws IllegalArgumentException;

    void withdraw(BigDecimal amount) throws IllegalArgumentException;
}
