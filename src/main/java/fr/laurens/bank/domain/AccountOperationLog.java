package fr.laurens.bank.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AccountOperationLog {

    private final List<AccountOperation> accountOperations = new ArrayList<>();

    public void addAccountOperation(AccountOperation accountOperation) {
        accountOperations.add(accountOperation);
    }

    public List<AccountOperation> getAccountOperations() {
        return Collections.unmodifiableList(accountOperations);
    }
}
