package fr.laurens.bank.domain;

public class AllowedOverdraftExceededException extends RuntimeException {

    public AllowedOverdraftExceededException(String message) {
        super(message);
    }
}
