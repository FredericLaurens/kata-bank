package fr.laurens.bank.domain;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Currency;
import java.util.Locale;

public class Account implements BalanceChangingOperations {

    private final static DecimalFormat AMOUNT_FORMAT;
    private final static String DEPOSIT = "deposit";
    private final static String WITHDRAW = "withdraw";

    static {
        AMOUNT_FORMAT = new DecimalFormat();
        AMOUNT_FORMAT.setMaximumFractionDigits(2);
        AMOUNT_FORMAT.setMinimumFractionDigits(2);
        AMOUNT_FORMAT.setCurrency(Currency.getInstance(Locale.FRANCE));
    }

    private final String accountNumber;
    private final Clock clock;
    private final AccountOperationLog accountOperationLog = new AccountOperationLog();
    private BigDecimal maxAllowedOverdraft = BigDecimal.valueOf(0);
    private BigDecimal balance = BigDecimal.valueOf(0);

    public Account(String accountNumber) {
        this.accountNumber = accountNumber;
        this.clock = Clock.getInstance();
    }

    public Account(String accountNumber, Clock clock) {
        this.accountNumber = accountNumber;
        this.clock = clock;
    }

    public AccountOperationLog getAccountOperationLog() {
        return this.accountOperationLog;
    }

    public BigDecimal balance() {
        return this.balance;
    }

    public BigDecimal maxAllowedOverdraft() {
        return this.maxAllowedOverdraft;
    }

    public void setMaxAllowedOverdraftTo(BigDecimal maxAllowedOverdraft) {
        checkForNegativeOrZeroAmount(maxAllowedOverdraft, "overdraft");
        this.maxAllowedOverdraft = maxAllowedOverdraft;
    }

    @Override
    public void deposit(BigDecimal amount) throws IllegalArgumentException {
        checkForNegativeOrZeroAmount(amount, DEPOSIT);
        balance = balance.add(amount);
        accountOperationLog.addAccountOperation(new AccountOperation(OperationType.DEPOSIT, amount, clock.now(), balance));
    }

    @Override
    public void withdraw(BigDecimal amount) throws IllegalArgumentException {
        checkForNegativeOrZeroAmount(amount, WITHDRAW);
        checkForMaxAllowedOverdraft(amount);
        balance = balance.subtract(amount);
        accountOperationLog.addAccountOperation(new AccountOperation(OperationType.WITHDRAWAL, amount, clock.now(), balance));
    }

    private void checkForMaxAllowedOverdraft(BigDecimal amount) {
        if (BigDecimal.ZERO.subtract(maxAllowedOverdraft).compareTo(balance.subtract(amount)) >= 0) {
            throw new AllowedOverdraftExceededException
                    (String.format("Cannot withdraw more than maximum allowed overdraft : %s", AMOUNT_FORMAT.format(maxAllowedOverdraft)));
        }
    }

    private void checkForNegativeOrZeroAmount(BigDecimal amount, String operation) {

        var result = BigDecimal.ZERO.compareTo(amount);

        if (result == 0) {
            throw new IllegalArgumentException(String.format("Cannot %s null amount", operation));
        }

        if (result > 0) {
            throw new IllegalArgumentException(String.format("Cannot %s negative amount : %s", operation, AMOUNT_FORMAT.format(amount)));
        }
    }
}
