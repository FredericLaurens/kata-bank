package fr.laurens.bank.persistence;

import fr.laurens.bank.domain.Account;

import java.util.Optional;

public interface AccountRepository {

    Optional<Account> findById(String accountNumber);
}
