package fr.laurens.bank.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.tuple;

public class AccountTest {

    private Account account;

    @BeforeEach
    public void createAccount() {
        account = new Account("ID");
    }

    @Test
    public void initial_amount_on_account_should_be_zero() {
        //then
        assertThat(account.balance()).isEqualTo(BigDecimal.ZERO);
        assertThat(account.getAccountOperationLog().getAccountOperations().size()).isEqualTo(0);
    }

    @Test
    public void should_deposit_positive_amount_onto_account() {

        //when
        account.deposit(BigDecimal.valueOf(20));
        //then
        assertThat(account.balance()).isEqualTo(BigDecimal.valueOf(20));
        assertThat(account.getAccountOperationLog().getAccountOperations().size()).isEqualTo(1);

        //when
        account.deposit(BigDecimal.valueOf(20.55));
        //then
        assertThat(account.balance()).isEqualTo(BigDecimal.valueOf(40.55));
        assertThat(account.getAccountOperationLog().getAccountOperations().size()).isEqualTo(2);

        //when
        account.deposit(BigDecimal.valueOf(0.5));
        //then
        assertThat(account.balance()).isEqualTo(BigDecimal.valueOf(41.05));
        assertThat(account.getAccountOperationLog().getAccountOperations().size()).isEqualTo(3);
        assertThat(account.getAccountOperationLog().getAccountOperations())
                .extracting("operationType", "amount", "date", "resultingBalance")
                .isEqualTo(List.of(
                        tuple(OperationType.DEPOSIT, BigDecimal.valueOf(20), LocalDate.now(), BigDecimal.valueOf(20)),
                        tuple(OperationType.DEPOSIT, BigDecimal.valueOf(20.55), LocalDate.now(), BigDecimal.valueOf(40.55)),
                        tuple(OperationType.DEPOSIT, BigDecimal.valueOf(0.5), LocalDate.now(), BigDecimal.valueOf(41.05))));
    }

    @Test
    public void should_not_deposit_negative_amount_onto_account() {

        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            //when
            account.deposit(BigDecimal.valueOf(-20.55));
        });

        //then
        assertThat(account.balance()).isEqualTo(BigDecimal.valueOf(0));
        assertThat(thrown.getMessage()).isEqualTo("Cannot deposit negative amount : -20,55");
        assertThat(account.getAccountOperationLog().getAccountOperations().size()).isEqualTo(0);
    }

    @Test
    public void should_not_deposit_zero_amount_onto_account() {

        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            //when
            account.deposit(BigDecimal.valueOf(0));
        });

        //then
        assertThat(account.balance()).isEqualTo(BigDecimal.valueOf(0));
        assertThat(thrown.getMessage()).isEqualTo("Cannot deposit null amount");
        assertThat(account.getAccountOperationLog().getAccountOperations().size()).isEqualTo(0);
    }

    @Test
    public void should_withdraw_positive_amount_from_account() {
        //given
        account.setMaxAllowedOverdraftTo(BigDecimal.valueOf(50));

        //when
        account.withdraw(BigDecimal.valueOf(20));
        //then
        assertThat(account.balance()).isEqualTo(BigDecimal.valueOf(-20));
        assertThat(account.getAccountOperationLog().getAccountOperations().size()).isEqualTo(1);

        //when
        account.withdraw(BigDecimal.valueOf(20.55));
        //then
        assertThat(account.balance()).isEqualTo(BigDecimal.valueOf(-40.55));
        assertThat(account.getAccountOperationLog().getAccountOperations().size()).isEqualTo(2);

        //when
        account.withdraw(BigDecimal.valueOf(0.5));
        //then
        assertThat(account.balance()).isEqualTo(BigDecimal.valueOf(-41.05));
        assertThat(account.getAccountOperationLog().getAccountOperations().size()).isEqualTo(3);
        assertThat(account.getAccountOperationLog().getAccountOperations())
                .extracting("operationType", "amount", "date", "resultingBalance")
                .isEqualTo(List.of(
                        tuple(OperationType.WITHDRAWAL, BigDecimal.valueOf(20), LocalDate.now(), BigDecimal.valueOf(-20)),
                        tuple(OperationType.WITHDRAWAL, BigDecimal.valueOf(20.55), LocalDate.now(), BigDecimal.valueOf(-40.55)),
                        tuple(OperationType.WITHDRAWAL, BigDecimal.valueOf(0.5), LocalDate.now(), BigDecimal.valueOf(-41.05))));
    }

    @Test
    public void should_not_withdraw_negative_amount_from_account() {
        //given
        account.setMaxAllowedOverdraftTo(BigDecimal.valueOf(50));

        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            //when
            account.withdraw(BigDecimal.valueOf(-0.55));
        });

        //then
        assertThat(account.balance()).isEqualTo(BigDecimal.valueOf(0));
        assertThat(thrown.getMessage()).isEqualTo("Cannot withdraw negative amount : -0,55");
        assertThat(account.getAccountOperationLog().getAccountOperations().size()).isEqualTo(0);
    }

    @Test
    public void should_not_withdraw_zero_amount_from_account() {

        //given
        account.setMaxAllowedOverdraftTo(BigDecimal.valueOf(50));

        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            //when
            account.withdraw(BigDecimal.valueOf(0));
        });

        //then
        assertThat(account.balance()).isEqualTo(BigDecimal.valueOf(0));
        assertThat(thrown.getMessage()).isEqualTo("Cannot withdraw null amount");
        assertThat(account.getAccountOperationLog().getAccountOperations().size()).isEqualTo(0);
    }

    @Test
    void maximum_allowed_overdraft_should_be_zero_by_default() {
        //then
        assertThat(account.maxAllowedOverdraft()).isEqualTo(BigDecimal.ZERO);
    }

    @Test
    void should_modify_maximum_allowed_overdraft() {
        //when
        account.setMaxAllowedOverdraftTo(BigDecimal.valueOf(100.5));
        //then
        assertThat(account.maxAllowedOverdraft()).isEqualTo(BigDecimal.valueOf(100.5));
    }

    @Test
    public void should_not_withdraw_amount_if_overdraft_is_excessive() {
        //given
        account.setMaxAllowedOverdraftTo(BigDecimal.valueOf(100.5));

        AllowedOverdraftExceededException thrown = Assertions.assertThrows(AllowedOverdraftExceededException.class, () -> {
            //when
            account.withdraw(BigDecimal.valueOf(100.6));
        });

        //then
        assertThat(account.balance()).isEqualTo(BigDecimal.valueOf(0));
        assertThat(thrown.getMessage()).isEqualTo("Cannot withdraw more than maximum allowed overdraft : 100,50");
        assertThat(account.getAccountOperationLog().getAccountOperations().size()).isEqualTo(0);
    }
}