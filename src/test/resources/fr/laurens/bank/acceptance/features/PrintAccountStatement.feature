
Feature: Client can make deposits and withdrawals and print an account statement

  Scenario: two deposits and one withdrawal on different days

    Given a client makes a deposit of 100 on 01/01/2020
    And a deposit of 200 on 02/01/2020
    And a withdrawal of 50 on 03/01/2020
    When he prints his account statement
    Then he would see:

      | Operation  |Date|Amount|Balance|
      | withdrawal |03/01/2020|50,00|250,00|
      | deposit    |02/01/2020|200,00|300,00|
      | deposit    |01/01/2020|100,00|100,00|